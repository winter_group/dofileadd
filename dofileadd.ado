**=============================
** @Author: Fabian Winter <fabianwinter>
** @Date:   2019-06-07T10:32:19+02:00
** @Email:  winter@coll.mpg.de
** @Last modified by:   fabianwinter
** @Last modified time: 2019-06-07T10:35:14+02:00
**=============================

program define dofileadd
        syntax using/ , [text(string) dofile(string)]

        mata: suffix = pathsuffix("`using'")
        mata: st_local("suffix", suffix)
        if "`dofile'" == "" {
            loc dofile "an unspecified do-file"
        }
        if "`suffix'" == ".tex" {
            if "`text'" == "" {         // Default text TEX
            local text "% This file was created in `dofile' at `=c(current_date)' `=c(current_time)' by `=c(username)' in Stata v`=c(version)'"
        }                               // Check that specified text is comment
            if substr("`text'",1,1) != "" & substr("`text'",1,1) != "%"  {  // check if speciefied text starts with comment
            local text "% `text'"
            }
        }
        if "`suffix'" == ".html" {
            if "`text'" == "" {         //Default text HTML
            local text "<!-- This file was created in `dofile' at `=c(current_date)' `=c(current_time)' by `=c(username)' in Stata v`=c(version)' -->"
            }                           // Check that specified text is comment
            if substr("`text'",1,1) != "" & substr("`text'",1,4) != "<!--"  {  // check if speciefied text starts with comment
            local text "<!-- `text' -->"
            }
        }


        tempname ho hi
        tempfile work

        file open `ho' using `work', w
        file write `ho' "`text'" _n
        file open `hi' using "`using'", r
        file read `hi' line

        while r(eof) == 0  {
                file write `ho' `"`macval(line)'"' _n
                file read `hi' line
        }

        file close _all
        copy `work' "`using'", replace

end
